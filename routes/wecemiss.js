const Router = require('koa-router');
const Moment = require('moment');

const RegisteModel = require('../models/wecemiss/registe_model');
const PaymentModel = require('../models/wecemiss/payment_model');
const SwitchContentsLib = require('../libs/switch_contents_lib');

const router = new Router();

router.get('/', (ctx, next) => {
  ctx.body = 'hello'; 
});

router.get('/get_registe_data', async (ctx) => {
	let data = ctx.request.query,
			page = parseInt(data['page']) || 1,
			search = {
				sDate: data['sDate'],
				eDate: data['eDate'],
				q: data['q'] || '',
				page: Math.ceil(page - 1)
			},
			cbListResult = await get_registe_list(search),
			cbTotalResult = await get_registe_total(search),
			result = {
				status: true,
				lists: cbListResult['data'],
				totals: Math.ceil(parseInt(cbTotalResult['data']) / 30)
			}


  ctx.body = result; 
});

router.get('/get_registe_by_id', async (ctx) => {
	let data = ctx.request.query
			result = await get_registe_by_id(data),
			registeInfo = {};

	result = result['data'][0] || [];
	if(result) {
		registeInfo = {
			nameCN: result['name'] || "",
  		nameEN: result['name_en'] || "",
  		citizenship: result['citizenship'] || "",
  		birthday: result['birthday'] || "",
  		birthplace: result['birthplace'] || "",
  		age: result['age'] || "",
  		reside: result['reside'] || "",
  		identity: result['identity'] || "",
  		postal_code: result['postal_code'] || "",
  		address: result['address'] || "",
  		tel: result['tel'] || "",
  		phone: result['phone'] || "",
  		email: result['email'] || "",
  		education: result['education'] || "",
  		profession: result['profession'] || "",
  		occupation: result['occupation'] || "",
  		language: result['language'] || "",
  		jobs: result['jobs'] || "",
  		height: result['height'] || "",
		  weight: result['weight'] || "",
		  chest: result['chest'] || "",
		  waist: result['waist'] || "",
		  buttocks: result['buttocks'] || "",
		  interest: result['interest'] || "",
		  introduce: result['introduce'] || "",
		  fb: result['fb'] || "",
		  youtube: result['youtube'] || "",
		  selfImages: JSON.parse(result['image_two']) || ['', ''],
		  passportImages:  JSON.parse(result['image']) || ['', '', '', '', '', ''],
		  guardianName: result['guardian_name'] || "",
		  guardianRelatio: result['guardian_relation'] || "",
		  guardianPostal: result['guardian_postal'] || "",
		  guardianAddress: result['guardian_address'] || "",
		  guardianPhone: result['guardian_phone'] || "",
		  guardianTel: result['guardian_tel'] || ""
		}
	}

  ctx.body = registeInfo; 
});

router.get('/get_payment_data', async (ctx) => {
	let data = ctx.request.query,
			page = parseInt(data['page']) || 1,
			search = {
				sDate: data['sDate'],
				eDate: data['eDate'],
				q: data['q'] || '',
				page: Math.ceil(page - 1)
			},
			cbListResult = await get_payment_list(search),
			cbTotalResult = await get_payment_total(search),
			result = {
				status: true,
				lists: cbListResult['data'],
				totals: Math.ceil(parseInt(cbTotalResult['data']) / 30)
			}


  ctx.body = result; 
});

router.post('/create/image', async (ctx) => {
	let data = ctx.request.body.data;
			result = await SwitchContentsLib.base64_to_image(data);
			
	ctx.body = {
		status: true,
		imagePath: result || ''
	};
})

router.post('/update_registe_data', async (ctx) => {
	let data = ctx.request.body;
			id = data['id'];

	data['selfImages'] = JSON.stringify(data['selfImages']);
	data['passportImages'] = JSON.stringify(data['passportImages']);

	delete data['id'];

	let result = await update_registe_data(id, data);
			
	ctx.body = result;
})

module.exports = router;

const get_registe_list = (search) => {
	return new Promise((resolve, reject) => {
		RegisteModel.get_list_data(search).subscribe((result) => {
			resolve(result);
		});
	});
}

const get_registe_total = (search) => {
	return new Promise((resolve, reject) => {
		RegisteModel.get_total_count(search).subscribe((result) => {
			resolve(result);
		});
	});
}

const get_registe_by_id = (search) => {
	return new Promise((resolve, reject) => {
		RegisteModel.get_single_data_by_id(search).subscribe((result) => {
			resolve(result);
		});
	});
}

const get_payment_list = (search) => {
	return new Promise((resolve, reject) => {
		PaymentModel.get_list_data(search).subscribe((result) => {
			resolve(result);
		});
	});
}

const get_payment_total = (search) => {
	return new Promise((resolve, reject) => {
		PaymentModel.get_total_count(search).subscribe((result) => {
			resolve(result);
		});
	});
}

const update_registe_data = (id, data) => {
	return new Promise((resolve, reject) => {
		let updateData = {
			name: data['nameCN'],
			name_en: data['nameEN'],
			citizenship: data['citizenship'],
			birthday: data['birthday'],
			birthplace: data['birthplace'],
			age: data['age'],
			reside: data['reside'],
			identity: data['identity'],
			guardian_name: data['guardianName'],
			guardian_relation: data['guardianRelatio'],
			guardian_postal: data['guardianPostal'],
			guardian_address: data['guardianAddress'],
			guardian_phone: data['guardianPhone'],
			guardian_tel: data['guardianTel'],
			postal_code: data['postal_code'],
			address: data['address'],
			tel: data['tel'],
			phone: data['phone'],
			email: data['email'],
			education: data['education'],
			profession: data['profession'],
			occupation: data['occupation'],
			language: data['language'],
			jobs: data['jobs'],
			height: data['height'],
			weight: data['weight'],
			chest: data['chest'],
			waist: data['waist'],
			buttocks: data['buttocks'],
			interest: data['interest'],
			introduce: data['introduce'],
			fb: data['fb'],
			youtube: data['youtube'],
			image: data['selfImages'],
			image_two: data['passportImages']
		}
		RegisteModel.update_data(id, updateData).subscribe((result) => {
			resolve(result);
		});
	});
}