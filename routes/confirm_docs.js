const Router = require('koa-router');
const Moment = require('moment');

const ConfirmDocsModel = require('../models/confirm_docs_model');

const router = new Router();

router.get('/', (ctx, next) => {
  ctx.body = 'hello'; 
});

router.post('/set_data', async (ctx, next) => {
	let data = ctx.request.body,
			cbResult = await insert_data(data);

  ctx.body = cbResult; 
});

router.post('/update_data', async (ctx, next) => {
	let data = ctx.request.body,
			id = data['id'];

	delete data['id'];

	let cbResult = await update_data(id, data);

  ctx.body = cbResult; 
});

router.get('/get_data', async (ctx, next) => {
	let data = ctx.request.query,
			page = parseInt(data['page']) || 1,
			search = {
				page: Math.ceil(page - 1)
			},
			cbListResult = await get_list_data(search),
			cbTotalResult = await get_total_data(),
			result = {
				status: true,
				lists: cbListResult['data'],
				totals: Math.ceil(parseInt(cbTotalResult['data']) / 30)
			}


  ctx.body = result; 
});

router.get('/get_single_data_by_id', async (ctx, next) => {
	let data = ctx.request.query,
			search = {
				id: parseInt(data['id'])
			},
			cbResult = await get_single_data_by_id(search);

  ctx.body = cbResult; 
});

module.exports = router;

const insert_data = (data) => {
	return new Promise((resolve, reject) => {
		data['visiable'] = 1;
		data['created_at'] = Moment().format('YYYY-MM-DD HH:mm:ss');
		ConfirmDocsModel.set_data(data).subscribe((result) => {
			resolve(result);
		});
	});
}

const update_data = (id, data) => {
	return new Promise((resolve, reject) => {
		ConfirmDocsModel.update_data(id, data).subscribe((result) => {
			resolve(result);
		});
	});
}

const get_list_data = (search) => {
	return new Promise((resolve, reject) => {
		ConfirmDocsModel.get_list_data(search).subscribe((result) => {
			resolve(result);
		});
	});
}

const get_single_data_by_id = (search) => {
	return new Promise((resolve, reject) => {
		ConfirmDocsModel.get_single_data_by_id(search).subscribe((result) => {
			let data = result['data'][0] || [];
			if(Object.keys(data).length > 0) {
				delete data['visiable'];
				delete data['created_at'];
			}
			resolve({
				status: true,
				data
			});
		});
	});
}

const get_total_data = () => {
	return new Promise((resolve, reject) => {
		ConfirmDocsModel.get_total_count().subscribe((result) => {
			resolve(result);
		});
	});
}