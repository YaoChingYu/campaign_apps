const Mysql = require('mysql');
const Rx = require('rxjs/Rx');

const constants = require('../../configs/constants');

class RegisteModel {
	constructor(options) {
    this.pool = Mysql.createPool({
      host: constants.mysqlOptions.host,
			user: constants.mysqlOptions.username,
			password: constants.mysqlOptions.password,
			database: 'wecemisstw'
    });
  }

  get_list_data(search) {
		return Rx.Observable.create((observe) => {
			this.pool.getConnection((err, connection) => {
				let whereKey = '',
						whereValue = [],
						sql = '';
				// where元件組成 ==== start
	      for (var key in search) { // 非日期，關鍵字，分頁
	        if(key !== 'page' && key !== 'sDate' && key !== 'eDate' && key !== 'q') {
	        	whereKey += (whereKey === '') ? ' WHERE ' + key + ' = ?' : ' AND ' + key + ' = ?';
	        	whereValue.push(search[key]);
	        }
	      }
	      if(search['q'] !== '') { // 若關鍵字有值
	      	whereKey += ((whereKey === '') ? ' where ' : ' and ') + 'name like ' + connection.escape('%' + search['q'] + '%') + ' OR tel like ' + connection.escape('%' + search['q'] + '%') + ' OR phone like ' + connection.escape('%' + search['q'] + '%') + '';
	      }
	      whereKey += ((whereKey === '') ? ' where ' : ' and ') + 'created_at between ? and ?'; // 日期有值
	      whereValue.push(search['sDate']);
	      whereValue.push(search['eDate']);
	    	whereKey += ' ORDER BY id DESC'; // 資料由新到舊排序
				if(!search['page']) search['page'] = 0;
    		whereKey += ' LIMIT ' + Math.ceil(30 * search['page']) + ',30'; // 擷取資料數量
    		// where元件組成 ==== end
				sql = "SELECT id, name, phone, email, guardian_name, guardian_phone, visible FROM registes" + whereKey;
				let query = connection.query(sql, whereValue, (err, rows) => {
							if(err) {
								return observe.next({
									status: false,
									message: '資料取得失敗',
									data: []
								});
							};
							observe.next({
								status: true,
								message: '',
								data: rows
							});

							connection.release();
						});
				// console.log(query.sql);
			});
		});
	}

	get_single_data_by_id(search) {
		return Rx.Observable.create((observe) => {
			this.pool.getConnection((err, connection) => {
				let sql = "SELECT * FROM registes WHERE id = ?",
						query = connection.query(sql, [search['id']], (err, rows) => {
							if(err) {
								return observe.next({
									status: false,
									message: '資料取得失敗',
									data: []
								});
							};
							observe.next({
								status: true,
								message: '',
								data: rows
							});

							connection.release();
						});
				// console.log(query.sql);
			});
		});
	}

	get_total_count(search) {
		return Rx.Observable.create((observe) => {
			this.pool.getConnection((err, connection) => {
				let whereKey = '',
						whereValue = [],
						sql = '';
				// where元件組成 ==== start
	      for (var key in search) { // 非日期，關鍵字，分頁
	        if(key !== 'page' && key !== 'sDate' && key !== 'eDate' && key !== 'q') {
	        	whereKey += (whereKey === '') ? ' WHERE ' + key + ' = ?' : ' AND ' + key + ' = ?';
	        	whereValue.push(search[key]);
	        }
	      }
	      if(search['q'] !== '') { // 若關鍵字有值
	      	whereKey += ((whereKey === '') ? ' where ' : ' and ') + 'name like ' + connection.escape('%' + search['q'] + '%') + ' OR tel like ' + connection.escape('%' + search['q'] + '%') + ' OR phone like ' + connection.escape('%' + search['q'] + '%') + '';
	      }
	      whereKey += ((whereKey === '') ? ' where ' : ' and ') + 'created_at between ? and ?'; // 若日期有值
	      whereValue.push(search['sDate']);
	      whereValue.push(search['eDate']);
	      // where元件組成 ==== end
				sql = "SELECT count(*) as count FROM registes" + whereKey;
				let query = connection.query(sql, whereValue, (err, rows) => {
							if(err) {
								return observe.next({
									status: false,
									message: '資料取得失敗',
									data: 0
								});
							};
							observe.next({
								status: true,
								message: '',
								data: rows[0]['count']
							});

							connection.release();
						});
				// console.log(query.sql);
			});
		})
	}

	update_data(id, data) {
		return Rx.Observable.create((observe) => {
			this.pool.getConnection((err, connection) => {
				let sql = "UPDATE registes SET ? WHERE id = ?",
						query = connection.query(sql, [data, id], (err, rows) => {
							if(err) {
								return observe.next({
									status: false,
									message: '資料更新失敗'
								});
							};
							observe.next({
								status: true,
								message: ''
							});

							connection.release();
						});
				// console.log(query.sql);
			});
		});
	}
}

module.exports = new RegisteModel();