const Mysql = require('mysql');
const Rx = require('rxjs/Rx');

const constants = require('../configs/constants');

class ConfirmDocsModel {
	constructor(options) {
    this.pool = Mysql.createPool({
      host: constants.mysqlOptions.host,
			user: constants.mysqlOptions.username,
			password: constants.mysqlOptions.password,
			database: constants.mysqlOptions.database
    });
  }

  set_data(data) {
		return Rx.Observable.create((observe) => {
			this.pool.getConnection((err, connection) => {
				let sql = "INSERT INTO campaigns SET ?",
						query = connection.query(sql, data, (err, rows) => {
							if(err) {
								return observe.next({
									status: false,
									message: '資料設定失敗'
								});
							};
							observe.next({
								status: true,
								message: ''
							});

							connection.release();
						});
				// console.log(query.sql);
			});
		});
	}

	update_data(id, data) {
		return Rx.Observable.create((observe) => {
			this.pool.getConnection((err, connection) => {
				let sql = "UPDATE campaigns SET ? WHERE id = ?",
						query = connection.query(sql, [data, id], (err, rows) => {
							if(err) {
								return observe.next({
									status: false,
									message: '資料更新失敗'
								});
							};
							observe.next({
								status: true,
								message: ''
							});

							connection.release();
						});
				// console.log(query.sql);
			});
		});
	}

  get_list_data(search) {
		return Rx.Observable.create((observe) => {
			this.pool.getConnection((err, connection) => {
				let whereKey = '',
						whereValue = [],
						sql = '';
	      for (var key in search) {
	        if(key !== 'page') {
	        	whereKey += (whereKey === '') ? ' WHERE ' + key + ' = ?' : ' AND ' + key + ' = ?';
	        	whereValue.push(search[key]);
	        }
	      }
	    	whereKey += ' ORDER BY id DESC';
				if(!search['page']) search['page'] = 0;
    		whereKey += ' LIMIT ' + Math.ceil(30 * search['page']) + ',30';
				sql = "SELECT id, campaginName, server, code, db, visiable FROM campaigns" + whereKey;
				let query = connection.query(sql, whereValue, (err, rows) => {
							if(err) {
								return observe.next({
									status: false,
									message: '資料取得失敗',
									data: []
								});
							};
							observe.next({
								status: true,
								message: '',
								data: rows
							});

							connection.release();
						});
				// console.log(query.sql);
			});
		});
	}

	get_single_data_by_id(search) {
		return Rx.Observable.create((observe) => {
			this.pool.getConnection((err, connection) => {
				let sql = "SELECT * FROM campaigns WHERE id = ?",
						query = connection.query(sql, [search['id']], (err, rows) => {
							if(err) {
								return observe.next({
									status: false,
									message: '資料取得失敗',
									data: []
								});
							};
							observe.next({
								status: true,
								message: '',
								data: rows
							});

							connection.release();
						});
				// console.log(query.sql);
			});
		});
	}

	get_total_count() {
		return Rx.Observable.create((observe) => {
			this.pool.getConnection((err, connection) => {
				let sql = "SELECT count(*) as count FROM campaigns",
						query = connection.query(sql, [], (err, rows) => {
							if(err) {
								return observe.next({
									status: false,
									message: '資料取得失敗',
									data: 0
								});
							};
							observe.next({
								status: true,
								message: '',
								data: rows[0]['count']
							});

							connection.release();
						});
				// console.log(query.sql);
			});
		})
	}
}

module.exports = new ConfirmDocsModel();