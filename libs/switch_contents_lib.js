const Fs = require('fs'),
      Moment = require('moment'),
      Md5 = require('crypto-md5/md5');

class SwitchContentsLib {

	base64_to_image(data) {
	  return new Promise((resolve, reject) => {
	    let temp = data.split(','),
	        fileType = temp[0].split('\/')[1].split('\;')[0];
	        
	    if(['jpge, jpg, gif, png'].indexOf(fileType) === -1) {
	      let fileName = Md5(temp[1] + Moment().valueOf(), 'hex') + '.' + fileType,
	          fileContent = Buffer.from(temp[1].replace(/\"/ig, ''), 'base64');

	      Fs.writeFile(process.cwd() + '/public/upload/' + fileName, fileContent, (err) => {
	        if (err)
	          return console.log(err);

	        resolve(fileName);
	      });
	    } else {
	    	resolve(false);
	    }
	  });
	}

}

module.exports = new SwitchContentsLib();