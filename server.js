const Koa = require('koa');
const BodyParser = require('koa-bodyparser');
const Mount = require('koa-mount');

const ConfirmDocs = require('./routes/confirm_docs');
const Wecemiss = require('./routes/wecemiss');

const app = new Koa();

app.use(BodyParser({
	formLimit: '10mb'
}));
app.use(Mount('/confirm_docs', ConfirmDocs.routes()));
app.use(Mount('/wecemiss', Wecemiss.routes()));

app.listen(3000);

console.log('server is started');